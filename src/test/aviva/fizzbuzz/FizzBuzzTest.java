package test.aviva.fizzbuzz;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aviva.fizzbuzz.FizzBuzz;


/**
 * @author akanksha saxena 
 * following Class is Test Class for FizzBuzz
 **/
public class FizzBuzzTest {
	FizzBuzz fizzbuzz;

	@BeforeClass
	public void setUp() {
		fizzbuzz = new FizzBuzz();
		fizzbuzz.setWedFlag(false);
	}

	@Test
	public void testNegativeNumber() {
		Assert.assertEquals(fizzbuzz.validateUserInput(-18), false);
	}

	@Test
	public void testPositiveNumber() {
		Assert.assertEquals(fizzbuzz.validateUserInput(2), true);
	}

	@Test
	public void testZeroNumber() {
		Assert.assertEquals(fizzbuzz.validateUserInput(0), false);
	}

	@Test
	public void processNumberTest() {
		ArrayList<String> output = fizzbuzz.processNumber(4);
		Assert.assertEquals(output.size(), 4);
	}

	@Test
	public void processNumberdivby3Test() {
		ArrayList<String> output = fizzbuzz.processNumber(3);
		Assert.assertEquals(output.size(), 3);
		Assert.assertEquals(output.get(2), "fizz");
	}

	@Test
	public void processNumberdivby5Test() {
		ArrayList<String> output = fizzbuzz.processNumber(5);
		Assert.assertEquals(output.size(), 5);
		Assert.assertEquals(output.get(4), "buzz");
	}

	@Test
	public void processNumberdivby15Test() {
		ArrayList<String> output = fizzbuzz.processNumber(15);
		Assert.assertEquals(output.size(), 15);
		Assert.assertEquals(output.get(14), "fizz buzz");
	}

	@Test
	public void testNumberHghThen1000() {
		Assert.assertEquals(fizzbuzz.validateUserInput(10000), false);
	}

	@Test
	public void testWednesday() {
		fizzbuzz.setWedFlag(true);
		ArrayList<String> output = fizzbuzz.processNumber(15);
		Assert.assertEquals(output.size(), 15);
		Assert.assertEquals(output.get(14), "wizz wuzz");
	}

	@Test
	public void testWednesdaydiv3() {
		fizzbuzz.setWedFlag(true);
		ArrayList<String> output = fizzbuzz.processNumber(3);
		Assert.assertEquals(output.size(), 3);
		Assert.assertEquals(output.get(2), "wizz");
	}

	@Test
	public void testWednesdaydiv5() {
		fizzbuzz.setWedFlag(true);
		ArrayList<String> output = fizzbuzz.processNumber(10);
		Assert.assertEquals(output.size(), 10);
		Assert.assertEquals(output.get(9), "wuzz");
	}
}
