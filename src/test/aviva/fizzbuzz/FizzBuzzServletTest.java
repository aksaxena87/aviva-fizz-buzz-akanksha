package test.aviva.fizzbuzz;

import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.TestNG;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aviva.fizzbuzz.FizzBuzzServlet;

public class FizzBuzzServletTest extends TestNG {

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@BeforeClass
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testServlet() throws Exception {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);

		when(request.getParameter("value")).thenReturn("3");

		when(response.getWriter()).thenReturn(pw);

		new FizzBuzzServlet().doPost(request, response);

		String result = sw.getBuffer().toString().trim();

		System.out.println("Result: " + result);

		Assert.assertEquals(true, result.contains("fizz"));
	}
}
