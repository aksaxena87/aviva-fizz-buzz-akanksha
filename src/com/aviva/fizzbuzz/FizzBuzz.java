package com.aviva.fizzbuzz;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author akanksha saxena following Class is Business Logic Class For Fizz Buzz
 *         Utility
 **/
public class FizzBuzz {
	Calendar date = Calendar.getInstance();
	boolean wedFlag = false;

	/**
	 * default constructor
	 * 
	 */
	public FizzBuzz() {
		if (date.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
			wedFlag = true;
		}
	}

	/**
	 * validating user input is a positive Integer between 1-1000
	 * 
	 * @param int
	 * @param returns
	 *            boolean
	 * 
	 */
	public boolean validateUserInput(int number) {
		if (number > 0 && number <= 1000) {
			return true;
		}
		return false;
	}

	/**
	 * business logic for Fizz Buzz implementation
	 * 
	 * @param int
	 * @param returns
	 *            ArrayList
	 * 
	 */
	public ArrayList<String> processNumber(int number) {

		ArrayList<String> outputList = new ArrayList<String>();

		for (int i = 1; i <= number; i++) {
			if (i % 3 == 0 && i % 5 == 0) {
				if (wedFlag)
					outputList.add(i - 1, "wizz wuzz");
				else
					outputList.add(i - 1, "fizz buzz");
			} else if (i % 3 == 0) {
				if (wedFlag)
					outputList.add(i - 1, "wizz");
				else
					outputList.add(i - 1, "fizz");
			} else if (i % 5 == 0) {
				if (wedFlag)
					outputList.add(i - 1, "wuzz");
				else
					outputList.add(i - 1, "buzz");
			} else {
				outputList.add(i - 1, Integer.toString(i));
			}
		}
		return outputList;
	}

	public void setWedFlag(boolean wedFlag) {
		this.wedFlag = wedFlag;
	}

}
