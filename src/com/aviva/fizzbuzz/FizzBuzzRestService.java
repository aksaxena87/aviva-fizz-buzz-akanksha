package com.aviva.fizzbuzz;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * 
 * @author akanksha saxena : Following is Restful WebService Class which
 *         implements a GET method and returns the xml of the processed Numbers
 * 
 */
@Path("/fizzbuzzrestservice")
public class FizzBuzzRestService {
	FizzBuzz fizzBuzz = new FizzBuzz();
	String buff = new String();

	@Path("{number}")
	@GET
	@Produces("application/xml")
	public String processNumber(@PathParam("number") int number) {

		String result = "For the number " + number + " following is the output";

		if (fizzBuzz.validateUserInput(number)) {

			ArrayList<String> fizzBuzzList = fizzBuzz.processNumber(number);
			for (String val : fizzBuzzList) {

				if (!val.contains("izz") && !val.contains("uzz")) {
					result += "<mynode>" + val + "</mynode>";
				}
				if (val.equals("fizz buzz")) {
					result += "<mynode>" + "fizz buzz" + "</mynode>";
				} else if (val.equals("wizz wuzz")) {
					result += "<mynode>" + "wizz wuzz" + "</mynode>";
				} else if (val.equals("fizz")) {
					result += "<mynode>" + val + "</mynode>";
				} else if (val.equals("wizz")) {
					result += "<mynode>" + val + "</mynode>";
				} else if (val.equals("buzz")) {
					result += "<mynode>" + val + "</mynode>";
				} else if (val.equals("wuzz")) {
					result += "<mynode>" + val + "</mynode>";
				}
			}

		}
		return "<output>" + result + "</output>";

	}
}
