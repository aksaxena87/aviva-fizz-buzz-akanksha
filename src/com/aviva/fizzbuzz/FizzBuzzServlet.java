package com.aviva.fizzbuzz;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FizzBuzzServlet
 * 
 * @author akanksha saxena
 */
@WebServlet(description = "Fizz Buzz Servlet", urlPatterns = { "/FizzBuzzServlet" })
public class FizzBuzzServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FizzBuzz fizzBuzz = new FizzBuzz();
	ArrayList<String> fizzBuzzList = new ArrayList<String>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FizzBuzzServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response) Processed output representation in HTML format
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String value = request.getParameter("value");
		PrintWriter out = response.getWriter();
		if (value.matches("\\d+")
				&& fizzBuzz.validateUserInput(Integer.parseInt(value))) {
			fizzBuzzList = fizzBuzz.processNumber((Integer.parseInt(value)));
			out.println("<html>");
			out.println("<head><title>Fizz Buzz Assignment</title></head>");
			out.println("<body>");
			out.println("<ul style=list-style-type:square>");
			for (String val : fizzBuzzList) {
				if (!val.contains("izz") && !val.contains("uzz")) {
					out.println("<li style= color:black>" + val + "</li>");
				} else if (val.equals("fizz buzz")) {
					out.println("<li style= color:blue>" + "fizz"
							+ "<font color=\"green\">" + " buzz" + "</li>");
				} else if (val.equals("wizz wuzz")) {
					out.println("<li style= color:blue>" + "wizz"
							+ "<font color=\"green\">" + " wuzz" + "</li>");
				} else if (val.equals("fizz")) {
					out.println("<li style= color:blue>" + val + "</li>");
				} else if (val.equals("wizz")) {
					out.println("<li style= color:blue>" + val + "</li>");
				} else if (val.equals("buzz")) {
					out.println("<li style= color:green>" + val + "</li>");
				} else if (val.equals("wuzz")) {
					out.println("<li style= color:green>" + val + "</li>");
				}
			}
			out.println("</ul>");
			out.println("</body>");
			out.println("</html>");
			out.close();
		} else {
			out.println("<html>");
			out.println("<head><title>Fizz Buzz Assignment</title></head>");
			out.println("<body>");
			out.println("<h1>Enter a valid number (1-1000)</h1>");
			out.println("</body>");
			out.println("</html>");
			out.close();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

}
