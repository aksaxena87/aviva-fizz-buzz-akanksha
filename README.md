# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Following is Fizz Buzz utility which supports 
        The user should be able to enter a positive integer
        You should print out, in a vertical list, all of the values between 1 and the entered value
        Where the number is divisible by 3, you should instead print ‘fizz’
        Where the number is divisible by 5, you should instead print 'buzz'
        Where the number is divisible by 3 AND 5, you should instead print ‘fizz buzz’
        the entered value is an integer between 1 and 1000
        'fizz' is printed in blue, and 'buzz' in green
        if today is Wednesday, the words 'wizz' and ‘wuzz' are substituted for 'fizz' and 'buzz
* Version : 1

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

After forking , clone it on local repo , Import it as dynamic web project(maven)
* Summary of set up
maven , tomcat server needed
* Configuration
add tomcat server in servers list of IDE and add the project as one of the configured ones.
* Dependencies
mentioned in POM.XML are only needed
* Database configuration
no db connections
* How to run tests
TestNG is required for running test cases , 
right click on class and run as testng
* Deployment instructions
Right click on project , do a maven clean and install , 
package as war and deploy on tomcat folder and start the tomcat server , 
For testing right click on input.html and run it on tomcat instance 
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
reach out to akanksha.saxena@iiitb.net
* Other community or team contact